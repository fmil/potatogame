var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    var npm_path = 'node_modules/';

    mix.copy([
        npm_path + 'bootstrap-sass/assets/javascripts/bootstrap.js',
        npm_path + 'jquery/dist/jquery.min.js',
/*        npm_path + 'vue/dist/vue.js',
        npm_path + 'vue-resource/dist/vue-resource.js',*/
    ], 'public/js/');

    mix.sass('app.scss', 'public/css/app.css');

    mix.scripts('main.js', 'public/js/main.js').scripts('admin.js', 'public/js/admin.js');
});
