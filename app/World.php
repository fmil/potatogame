<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class World extends Model
{
    protected $table = 'world';
    protected $fillable = [
       'world_name', 'description', 'world_vertical', 'world_horizontal'
    ];

    public function worldRegions() {
        return $this->hasMany('App\WorldRegions', 'region_id');
    }

}
