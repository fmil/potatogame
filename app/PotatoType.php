<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PotatoType extends Model
{
    protected $table = 'potato_type';
    protected $fillable = [
      'potato_name', 'potato_description', 'buying_price', 'selling_price', 'weight', 'growth_ratio', 'rotting_speed', 'sub_products', 'available_level'
    ];
}
