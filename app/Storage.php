<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $table = 'storage';
    protected $fillable = [
        'owner_id', 'protection_level', 'damage_status', 'update_level', 'storage_space', 'area_id'
    ];


}
