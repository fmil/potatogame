<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketType extends Model
{
    //
    protected $table = 'market_type';

    protected $fillable = [
      'market_name', 'market_description', 'bonus_ratio', 'available_level'
    ];

}
