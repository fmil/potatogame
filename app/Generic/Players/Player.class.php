<?php 

namespace App\Generic\Players;

use App\PlayerClasses as PlCl;
use App\User;

class Player {

    private $_userId = 0;
    private $_userName = "";
    private $_userEmail = "";
    private $_screenName = "";
    private $_password = "";

    private $_userColour = "#263238";
    private $_userClass = "";

    private $_userExperience = 0;
    private $_userLevel = 0;

    private $_isVip = 0;
    private $_accessLevel = 0;

    private $_profileImageUrl = "";
    private $_cash = 1000;
    private $_goldPotatoes = 0;

    /*
    User class params
    */
    private $_strength = 0;
    private $_dexterity = 0;
    private $_intelligence = 0;
    private $_charisma = 0;

    /*
    Set user values 
    */

    public function setUserId($id = null) {
        return $this->_userId = is_null($id) ? $this->_userId : $id;
    }

    public function setUserName($name = null) {
        return $this->_userName = is_null($name) ? $this->_userName : $name;
    }

    public function setUserEmail($email = null) {
        return $this->_userEmail = is_null($email) ? $this->_userEmail : $email;
    }

    public function setScreenName($screenName = null){
        return $this->_screenName = is_null($screenName) ? $this->_userName : $screenName;
    }

    public function setPassword($psw = null) {
        return $this->_password = bcrypt($psw);
    }

    public function setUserColour($colour = null){
        return $this->_userColour = is_null($colour) ? $this->_userClass : $colour;
    }

    public function setUserClass($class = null) {
        return $this->_userClass = is_null($class) ? $this->_userClass : $class; 
        //TODO: change to actual PlayerClass.class.php
    }

    public function setUserExperience($experience = null) {
        return $this->_userExperience = is_null($experience) ? $this->_userExperience: $experience;
    }

    public function setUserLevel($lvl = null) {
        return $this->_userLevel = is_null($lvl) ? $this->_userLevel : $lvl;
    }

    public function setIsVip($isVip = null) {
        return $this->_isVip = is_null($isVip) ? $this->_isVip :  $isVip;
    }

    public function setAccessLevel($accessLevel = null) {
        return $this->_accessLevel = is_null($accessLevel) ? $this->_accessLevel : $accessLevel;
    }

    public function setUserImageUrl($url = null) {
        return $this->_profileImageUrl = is_null($url) ? $this->_profileImageUrl : $url;
    }

    public function setUserCash($cash = null) {
        return $this->_cash = is_null($cash) ? $this->_cash : $cash;
    }

    public function setGoldenPotatoes($gold = null) {
        return $this->_goldPotatoes = is_null($gold) ? $this->_goldPotatoes : $gold;
    }

    public function setStrength($str = null) {
        return $this->_strength = is_null($str) ? $this->_strength : $str; 
    }

    public function setDexterity($dxt = null) {
        return $this->_dexterity = is_null($dxt) ? $this->_dexterity : $dxt;
    }

    public function setIntelligence($intel = null) {
        return $this->_intelligence = is_null($intel) ? $this->_intelligence : $intel;
    }

    public function setCharisma($charism = null) {
        return $this->_charisma = is_null($charism) ? $this->_charisma : $charism;
    }

    /* 
    User getters 
    */

    public function getUserId() {
        return $this->_userId;
    }

    public function getUserName() {
        return $this->_userName;
    }

    public function getUserEmail() {
        return $this->_userEmail;
    }

    public function getScreenName(){
        return $this->_screenName;
    }

    public function getUserColour(){
        return $this->_userColour;
    }

    public function getUserClass() {
        return $this->_userClass;
    }

    public function getUserExperience() {
        return $this->_userExperience;
    }

    public function getUserLevel() {
        return $this->_userLevel;
    }

    public function getIsVip() {
        return $this->_isVip;
    }

    public function getAccessLevel() {
        return $this->_accessLevel;
    }

    public function getUserImageUrl() {
        return $this->_profileImageUrl;
    }

    public function getUserCash() {
        return $this->_cash;
    }

    public function getGoldenPotatoes() {
        return $this->_goldPotatoes;
    }

    public function getStrength() {
        return $this->_strength;
    }

    public function getDexterity() {
        return $this->_dexterity;
    }

    public function getIntelligence() {
        return $this->_intelligence;
    }

    public function getCharisma() {
        return $this->_charisma;
    }

    public function __construct() {
    }

    /* 
    Basic functions 
    */ 

    public function usernameAvailable() {
        $user = User::where('user_name', '=', $this->_userName)->first();
        return ($user) ? true : false;
    }

    public function emailAvailable() {
        $email = User::where('email', '=', $this->_userEmail)->first();
        return ($email) ? true : false;
    }

    public function createUser(){

        $user = User::create([
            'user_name' => $this->_userName,
            'email' =>  $this->_userEmail,
            'screen_name' => $this->_screenName,
            'player_class' => $this->_userClass,
            'password' => $this->_password,
            'strength' => $this->_strength,
            'dexterity' => $this->_dexterity,
            'intelligence' => $this->_intelligence,
            'charisma' => $this->_charisma
        ]);

        /*
        TODO: ADD validators and try catch 
        */

        return $this->setUserId($user["user_id"]);
    }

    public function setBaseParamsFromClassId() {

        $getClass = PlCl::where("player_class_id", "=", $this->_userClass)->first();

        $this->setStrength($getClass["strength"]);
        $this->setDexterity($getClass["dexterity"]);
        $this->setIntelligence($getClass["intelligence"]);
        $this->setCharisma($getClass["charisma"]);

        return;
    }

}

?>