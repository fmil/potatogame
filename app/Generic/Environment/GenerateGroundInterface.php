<?php 

namespace App\Generic\Environment;

interface GenerateGroundInterface {

    /*World/Region generation*/
    public function generate();
    /*Delete World/Region*/
    public function delete();
    /* Disable World/Region */
    public function disable();

}

?>