<?php 


namespace App\Generic\GameObjects;

class GameValues { 


    private $_amount = 0;
    private $_reason = "";

    public function setAmount($amount = null) {
        return $_amount = is_null() ? $this->_amount : $amount;
    }

    public function setReason($reason = null) {
        return $_reason = is_null($reason) ? $this->_reason : $reason;
    }

    public function getAmount() {
        return $this->_amount;
    }

    public function getReason() {
        return $this->_reason;
    }

    public function bonusMoney(CurrencyInterface $money) {
        //ADD SOME CALCUALTIONS IF NEEDED 
        return $money->addFunds($this->_amount);
    }

    public function losePenalty(CurrencyInterface $money) {
        //ADD OTHER CALCULATIOSN IF NEEDED
        return $money->removeFunds($this->_amount);
    }

    
}