<?php 

namespace App\Generic\GameObjects;


class Potato { 

    private $_potatoId = 0;
    private $_potatoName = "";
    private $_potatoDescription = "";

    private $_sellingPrice = 2;
    private $_buyingPrice = 1;

    private $_weight = 0;
    private $_growthRatio = 0;
    private $_rottingSpeed = 0;

    private $_subProducts = array();
    private $_availableLevel = 0;


    /* SETTERS */

    public function setPotatoId($id = null) {
        $this->_potatoId = is_null($id) ? $this->_potatoId : $id;
        return $this->_potatoId;
    }

    public function setPotatoName($name = null) {
        $this->_potatoName = is_null($this->_potatoName) ? $this->_potatoName : $name;
        return $this->_potatoName;
    }

    public function setPotatoDescription($description = null){
        $this->_potatoDescription = is_null($description) ? $this->_potatoDescription : $description;
        return $this->_potatoDescription;
    }

    public function setSellingPrice($price = null){
        $this->_sellingPrice = is_null($price) ? $this->_sellingPrice : $price;
        return $this->_sellingPrice;
    }

    public function setBuyingPrice($price = null) {
        $this->_buyingPrice = is_null($price) ? $this->_buyingPrice : $price;
        return $this->_buyingPrice;
    }

    public function setWeight($weight = null) {
        $this->_weight = is_null($weight) ? $this->_weight : $weight;
        return $this->_weight;
    }

    public function setGrowthRatio($ratio = null){ 
        $this->_growthRatio = is_null($ratio) ? $this->_growthRatio : $ratio;
        return $this->_growthRatio;
    }

    public function setRottingSpeed($speed = null) {
        $this->_rottingSpeed = is_null($speed) ? $this->_rottingSpeed : $speed ;
        return $this->_rottingSpeed;
    }

    public function setSubProducts($prod = null) {
        $this->_subProducts = is_null($prod) ? $this->_subProducts : $prod;
        return $this->_subProducts;
    }

    public function setAvailableLevel($level = null) {
        $this->_availableLevel = is_null($level) ? $this->_availableLevel : $level;
        return $this->_availableLevel;
    }

    /* GETTERS */

    public function getPotatoId() {
        return $this->_potatoId;
    }

    public function getPotatoName() {
        return $this->_potatoName;
    }

    public function getPotatoDescription() {
        return $this->_potatoDescription;
    }

    public function getSellingPrice() {
        return $this->_sellingPrice;
    }

    public function getBuyingPrice() {
        return $this->_buyingPrice;
    }

    public function getWeight() {
        return $this->_weight;
    }

    public function getGrowthRatio() {
        return $this->_growthRatio;
    }

    public function getRottingSpeed() {
        return $this->_rottingSpeed;
    }

    public function getSubProducts() {
        return $this->_subProducts;
    }  

    public function getAvailableLevel() {
        return $this->_availableLevel;
    }

    public function __construct() {

    }


}



?>