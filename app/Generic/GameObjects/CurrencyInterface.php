<?php 



namespace App\Generic\GameObjects;

interface CurrencyInterface { 

    /* Add needed funds */
    public function addFunds($amount);
    /* Remove funds */
    public function removeFunds($amount);

}