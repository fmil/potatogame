<?php 

namespace App\Generic\Helpers;

interface ValidatorInterface {

    public function validate();

    public function addRule();
}


?>