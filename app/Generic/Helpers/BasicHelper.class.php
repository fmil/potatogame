<?php


namespace App\Generic\Helpers;

class BasicHelper {

    private $_msg = "";

    public function setMessage($msg = null) {
        return $this->_msg = is_null($msg) ? $this->_msg : $msg;
    }

    public function getMessage() {
        return $this->_msg;
    }

    public function respondWithError () {

    }

    public function respondWithSuccess() {
        
    }

}


?>