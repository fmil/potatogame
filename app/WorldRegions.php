<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorldRegions extends Model
{
    //
    protected $table = 'world_regions';
    protected $fillable = [
      'region_name', 'region_short_code', 'world_id', 'unused_land', 'used_land', 'world_hor', 'world_ver'
    ];

    public function world(){
        return $this->belongsTo('App\World');
    }

}
