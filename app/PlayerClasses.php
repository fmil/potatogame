<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerClasses extends Model
{
    protected $table = 'player_classes';
    protected $fillable = [
      'name', 'short_description', 'long_description', 'strength', 'dexterity', 'intelligence', 'charisma'
    ];

}
