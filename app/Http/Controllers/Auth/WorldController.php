<?php

namespace App\Http\Controllers\Auth;

use App\RegionAreas;
use App\World;
use App\WorldRegions;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthController;

class WorldController extends AuthController
{

    public function mapSettings() {

        $has_world = true;
        $world = World::all();
        $region_list = [];


        if(count($world) <= 0) {
            $has_world = false;
        } else {
            foreach ($world as $wr) {
                $reg = WorldRegions::where('world_id', '=', $wr['world_id'])->get();
                $region_list[$wr['world_id']] = $reg;
            }
        }

        $areas = RegionAreas::all();
        $area_list = [];
        foreach ($areas as $ar) {
            $area_list[$ar['region_id'].'/'.$ar['hor_cords'].'/'.$ar['ver_cords']] = $ar;
        }


        $response = [
            'has_world'=>$has_world,
            'worlds'=>$world,
            'regions'=>$region_list,
            'region_areas'=>$area_list
        ];

        return view("admin.pages.map")->with('response', $response);
    }

    public function newWorld(Request $request) {

        $req = $request->all();

        try {

            $world = World::where('world_name', '=', $req['world_name'])->first();

            if($world) {
                throw new \Exception('This world already exist.');
            }

            World::create($req);

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        flash('New world has been created', 'success');
        return redirect()->back();
    }

    public function newRegion(Request $request) {

        //Find location World can have 12 width and 12 down. It means. World can have maximum - 144 regions with total (max 100 users per region) 144 000 Users in world
        //'region_name', 'region_short_code', 'world_id', 'unused_land', 'used_land', 'world_hor', 'world_ver'

        $req = $request->all();

        try {
            $world = World::where('world_id', '=', $req['world_id'])->first();

            //get short code for new region
            $region_short_code  = $this->shortCodeGenerator($world['world_name'], $req['region_name'], $req['region_short_code']);

            if($region_short_code['status'] == 'error') {
                throw new \Exception('This short code already exist.');
            }

            //setting region coordinates 'world_vertical', 'world_horizontal'
            if($world['world_horizontal'] == 12) {
                $world_hor = 1;
                $world_ver = $world['world_vertical']+1;
            } else {
                $world_hor = $world['world_horizontal']+1;
                if($world['world_vertical'] == 0) {
                    $world_ver = 1;
                } else {
                    $world_ver = $world['world_vertical'];
                }
            }

            if($world['world_horizontal'] == 12 && $world['world_vertical'] == 12) {
                throw new \Exception('This world is overpopulated. Create new one.');
            }

            WorldRegions::create([
                'region_name'=>$req['region_name'],
                'region_short_code'=>$region_short_code['short'],
                'world_id'=>$req['world_id'],
                'world_hor'=>$world_hor,
                'world_ver'=>$world_ver
            ]);

            //update position to the world
            World::where('world_id', '=', $req['world_id'])->update(['world_horizontal'=>$world_hor,'world_vertical'=>$world_ver]);

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        flash('New region is created.', 'success');
        return redirect()->back();
    }


    protected function shortCodeGenerator($world, $regionName, $short = null){

        //Pattern is [First 2 letters from world name, 2 digits, first 2 letters from region name]

        $letters = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        $isUnique = false;

        if(isset($short) || !is_null($short)) {

            $checkShort = WorldRegions::where('region_short_code', '=', $short)->first();
            if(!$checkShort) {
                return ['status'=>'success', 'short'=>$short];
            } else {
                return ['status'=>'error'];
            }

        }

        //dd($world);

        while($isUnique == false) {
            $shortCode = "";
            if(strlen($world) < 2) {
                $shortCode = strtoupper($world.$letters[rand(0, count($letters)-1)]);
            } else {
                $shortCode = strtoupper(substr($world, 0, 2));
            }

            $shortCode = $shortCode.rand(0,9).rand(0,9);

            if(strlen($regionName) <2) {
                $shortCode = strtoupper($shortCode.$regionName.$letters[rand(0, count($letters)-1)]);
            } else {
                $shortCode = strtoupper($shortCode.substr($regionName, 0, 2));
            }

            $shortCode = $shortCode.rand(0,9);
            $checkShort = WorldRegions::where('region_short_code', '=', $shortCode)->first();

            if(is_null($checkShort)) {
                $isUnique = true;
            }

        }

        return ['status'=>'success', 'short'=>$shortCode];
    }


}
