<?php

namespace App\Http\Controllers\Auth;

use App\Backpack;
use App\PotatoField;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthController;
use Auth;

class ProfileController extends AuthController
{
    public function profile(){

        $user = Auth::user();

        //return all potato fields
        $potatoFields = PotatoField::where('owner_id', '=', $user->user_id)->get();

        //return backpack_info
        $_tempBackpack = Backpack::where('owner_id', '=', $user->user_id)->get();

        $backpack = [];

        foreach ($_tempBackpack as $item) {
            if($item["item_type"] == 'potato') {
                $_potato = $item->potatoType();
                $backpack["potatoes"][$_potato['potato_id']] = $item;
            } else {
                $backpack[$item['item_name']] = $item;
            }
        }

        $response = [
            'potatoFields' => $potatoFields,
            'backpack' => $backpack
        ];

        return view('auth.pages.profile')->with('response', $response);
    }

    public function profileImage(){
        //TODO: move file upload to profile section
        /*            if(isset($req['profile_image'])) {
                        $image = $_FILES['profile_image']; //$req['profile_image'];
                        if($image['size'] >= 17777) {
                            throw new \Exception('File size is too large (Max 2 MB)');
                        }

                        //TODO: add check for file type
                    }*/
    }
}
