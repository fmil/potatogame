<?php

namespace App\Http\Controllers\Auth;

use App\MarketType;
use App\PlayerClasses;
use App\PotatoType;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthController;

class AdminController extends AuthController
{
    public function main(){

        return view('admin.pages.main');
    }

    public function createPotato(Request $request) {

        $req = $request->all();

        try {
            $potato = PotatoType::where('potato_name', '=', $req['potato_name'])->first();

            if($potato) {
                throw new \Exception("This potato type already exist.");
            }

            PotatoType::create($req);

        } catch (\Exception $e) {
            flash($e->getMessage(), "danger")->important();
            return redirect()->back();
        }

        flash("New potato has been created.", "success");
        return redirect()->back();
        //'potato_name', 'potato_description', 'buying_price', 'selling_price', 'weight', 'growth_ratio', 'rotting_speed', 'sub_products'
    }

    public function createMarket(Request $request) {

        $req = $request->all();

        try {
            $market = MarketType::where('market_name', '=', $req['market_name'])->first();
            if($market) {
                throw new \Exception('This market type already exist.');
            }

  /*          $req['bonus_ratio'] = !isset($req['bonus_ratio']) ? 0 : $req['bonus_ratio'];*/

            MarketType::create($req);

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        flash('New market has been created.', 'success');
        return redirect()->back();

    }

    public function createUserClass(Request $request) {

        $req = $request->all();

        try {

            $playerClass = PlayerClasses::where('name', '=', $req['name'])->first();

            if($playerClass) {
                throw new \Exception('This player class already exist.');
            }

            PlayerClasses::create($req);

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        flash('Player class has been created.', 'success');
        return redirect()->back();
    }

    public function generalSettings(){

        $potato = PotatoType::all();
        $markets = MarketType::all();
        $playerClasses = PlayerClasses::all();

        $response = [
            'potato'=>$potato,
            'markets'=>$markets,
            'userclasses'=>$playerClasses
        ];

        return view("admin.pages.general")->with('response', $response);
    }

}
