<?php

namespace App\Http\Controllers\Auth;

use App\PotatoType;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthController;
use Auth;

class MarketController extends AuthController
{
    public function market(){
        /*
            Need 2 groups - selling and buying
        */
        $_potatoes = PotatoType::all();

        $response = [
          "selling" => [
              "potatoes" => $_potatoes
          ]
        ];

        return view("auth.pages.market")->with("response", $response);
    }
}
