<?php

namespace App\Http\Controllers;

use App\PlayerClasses;
use App\PotatoField;
use App\RegionAreas;
use App\Storage;
use App\User;
use App\WorldRegions;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Auth;
use App\Generic\Players\Player;

class GuestController extends Controller
{

    public function login (){
        return view("guest.pages.login");
    }

    public function loginUser(Request $request){

        $req = $request->all();

        if(Auth::attempt(['user_name'=>$req['username'], 'password'=>$req['password']])) {

            flash('Welcome '.Auth::user()->screen_name.' to Potato land!', 'success');
            return redirect('/profile');
        }

        flash('Something is wrong with your login details.', 'danger')->important();
        return redirect()->back();
    }

    public function register() {

        $playerClasses = PlayerClasses::all();

        $response = [
          'classes' =>$playerClasses
        ];

        return view("guest.pages.register")->with('response', $response);
    }

    public function registerUser(Request $request) {

        $req = $request->all();

        try {

            $player = new Player();
            $player->setUserName($req["username"]);
            $player->setUserEmail($req["email"]);
            $player->setScreenName($req["screenname"]);
            $player->setUserClass($req["farmer_class"]);
            $player->setPassword($req["password"]);
            
            $player->setBaseParamsFromClassId();

            if($player->usernameAvailable()) {
                throw new \Exception('This username is already taken.');
            }

            if($player->emailAvailable()){
                throw new \Exception('This email is already taken.');
            }

            //check if username taken
            /*
            $hasUser = User::where('user_name', '=', $req['username'])->first();
            */
            //TODO: Create this check in user side (so he can see while he is filling the form)
            /*
            if($hasUser) {
                throw new \Exception('This username is already taken.');
            }
            */
/*
            $hasEmail = User::where('email', '=', $req['email'])->first();
            if($hasEmail) {
                throw new \Exception('This email is already taken.');
            }
*/
            //load class of Player type
            //$class = PlayerClasses::where('player_class_id', '=', $req['farmer_class'])->first();

            $userId = $player->createUser();

            //generate place in the map
            //TODO: add check if all regions are full. Send error : All servers are full. or something.
            $region = WorldRegions::where('used_land', '<', 100)->orderBy('used_land', 'asc')->first();

            $live_place = $this->findArea($userId, $region);

            //Create first field/storage
            $field = new PotatoField();
            $field->owner_id = $userId;
            $field->area_id = $live_place;
            $field->save();

            $storage = new Storage();
            $storage->owner_id = $userId;
            $storage->area_id = $live_place;
            $storage->save();

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        Auth::loginUsingId($userId, true);
        //Auth::login($user);

        flash('Welcome to the "Potato Kings"', 'success');
        return redirect('/profile');
    }

    protected function findArea($id, WorldRegions $region){


        $isAvailable = true;

        while($isAvailable){

            $cord_hor = rand(1,10);
            $cord_ver = rand(1,10);

            $area = RegionAreas::where('region_id', '=', $region['region_id'])
                    ->where('hor_cords', '=', $cord_hor)
                    ->where('ver_cords', '=', $cord_ver)->first();

            if(is_null($area)) {
                $isAvailable = false;
                $newRegion = RegionAreas::create([
                    'region_id'=>$region['region_id'],
                    'owner_id'=>$id,
                    'region_short_code'=>$region['region_short_code'],
                    'hor_cords'=>$cord_hor,
                    'ver_cords'=>$cord_ver
                ]);
            }
        }

        WorldRegions::where('region_id', '=', $region['region_id'])->update(['unused_land'=>($region['unused_land']-1), 'used_land'=>($region['used_land']+1)]);

        return $newRegion['area_id'];
    }

}
