<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PotatoField extends Model
{
    protected $table = 'potato_fields';

    protected $fillable = [
        'owner_id', 'width', 'height', 'has_potatoes', 'growth', 'growth_ratio', 'rotten_level', 'field_level', 'potato_id','area_id'
    ];
}
