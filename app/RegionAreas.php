<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionAreas extends Model
{
    //
    protected $primaryKey = 'area_id';

    protected $table = 'region_areas';
    protected $fillable = [
      'owner_id', 'region_id', 'region_short_code', 'alliance_id', 'hor_cords', 'ver_cords', 'is_fake'
    ];

}
