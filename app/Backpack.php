<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backpack extends Model
{

    protected $table = 'backpack';
    protected $primaryKey = 'backpack_id';

    protected $fillable = [
      'owner_id', 'item_name', 'item_count', 'item_unite', 'item_type', 'item_subtype'
    ];

    public function user(){
        return $this->hasOne('App\User', 'user_id', 'owner_id');
    }

    //return potato info
    public function potatoType(){
        return $this->hasOne('App\PotatoType', 'potato_name', 'item_name');
    }
}
