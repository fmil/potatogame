<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* this is guest stuff */

Route::group(['middleware' => 'guest'], function(){
    Route::get('/', "GuestController@login");
    Route::post('/login', 'GuestController@loginUser');

    Route::get('/register', 'GuestController@register');
    Route::post('/register', 'GuestController@registerUser');
});


/*this will be auth group*/

Route::group(['middleware'=>['auth']], function(){
    Route::get('/profile', 'Auth\ProfileController@profile');
    Route::get('/market', 'Auth\MarketController@market');
});


Route::group(['middleware'=>['auth','admin']], function(){
    /*this will be admin group controllers */
    Route::get('/potatogod', 'Auth\AdminController@main');
    Route::get('/potatogod/general', 'Auth\AdminController@generalSettings');
    Route::get('/potatogod/map', 'Auth\WorldController@mapSettings');
    // potato management
    Route::post('/potatogod/potato', 'Auth\AdminController@createPotato');
    Route::post('/potatogod/usersclass', 'Auth\AdminController@createUserClass');
    Route::post('/potatogod/market', 'Auth\AdminController@createMarket');
    // world manager
    Route::post('/potatogod/world', 'Auth\WorldController@newWorld');
    Route::post('/potatogod/region', 'Auth\WorldController@newRegion');
});



