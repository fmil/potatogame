<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatsToPlayerClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_classes', function (Blueprint $table) {
            $table->integer('strength');
            $table->integer('dexterity');
            $table->integer('intelligence');
            $table->integer('charisma');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_classes', function (Blueprint $table) {
            $table->dropColumn('strength');
            $table->dropColumn('dexterity');
            $table->dropColumn('intelligence');
            $table->dropColumn('charisma');
        });
    }
}
