<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePotatoTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potato_type', function (Blueprint $table) {
            $table->increments('potato_id');
            $table->string('potato_name');
            $table->text('potato_description')->nullable();
            $table->double('selling_price')->default(0);
            $table->double('buying_price')->default(0);
            $table->double('weight')->default(1); //how much you get from 1 seed
            $table->double('growth_ratio')->default(1);
            $table->double('rotting_speed')->default(1);
            $table->string('sub_products')->nullable(); // products what you need additional for potato growth
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potato_type');
    }
}
