<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorldRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('world_regions', function (Blueprint $table) {
            $table->increments('region_id');
            $table->string('region_name');
            $table->string('region_short_code')->unique();
            $table->integer('world_id')->unsigned();
            $table->foreign('world_id')
                ->references('world_id')
                ->on('world')
                ->onDelete('cascade');
            $table->integer('unused_land')->default(100);
            $table->integer('used_land')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('world_regions');
    }
}
