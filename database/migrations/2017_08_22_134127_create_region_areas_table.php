<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region_areas', function (Blueprint $table) {
            $table->increments('area_id');
            $table->integer('owner_id')->nullable();
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')
                ->references('region_id')
                ->on('world_regions')
                ->onDelete('cascade');
            $table->string('region_short_code');

            $table->integer('alliance_id')->nullable();
            $table->integer('hor_cords')->default(1);
            $table->integer('ver_cords')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region_areas');
    }
}
