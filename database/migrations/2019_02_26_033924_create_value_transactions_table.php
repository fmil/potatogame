<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValueTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_transactions', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->integer('type_id')->unsigned();

            $table->foreign('type_id')
            ->references('transaction_type_id')
            ->on('transaction_types')
            ->onDelete('cascade');

            $table->float('amount')->default(0);
            $table->string('reason')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
            ->references('user_id')
            ->on('users')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('value_transactions');
    }
}
