<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_type', function (Blueprint $table) {
            $table->increments('market_id');
            $table->string('market_name');
            $table->text('market_description')->nullable();
            $table->double('bonus_ratio')->default(0);
            $table->integer('available_level')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_type');
    }
}
