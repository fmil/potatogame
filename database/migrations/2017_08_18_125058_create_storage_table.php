<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage', function (Blueprint $table) {
            $table->increments('storage_id');
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')
                ->references('user_id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('protection_level')->default(0);
            $table->double('damage_status')->default(0); //shows percentage of damages made to storage
            $table->integer('update_level')->default(1);
            $table->integer('storage_space')->default(100); //kg of storage * multiplied with level of storage ?

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storage');
    }
}
