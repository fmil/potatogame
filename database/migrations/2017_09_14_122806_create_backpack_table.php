<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackpackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backpack', function (Blueprint $table) {
            $table->increments('backpack_id');
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')
                ->references('user_id')
                ->on('users')
                ->onDelete('cascade');

            $table->string('item_name');
            $table->double('item_count')->default(0);
            $table->string('item_unite')->default('kg');
            $table->string('item_type')->default('potato');
            $table->string('item_subtype')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backpack');
    }
}
