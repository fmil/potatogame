<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvailableFromFieldToPotato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('potato_type', function (Blueprint $table) {
            $table->integer('available_level')->default(1); //level from which you can buy this potato
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('potato_type', function (Blueprint $table) {
            $table->dropColumn('available_level');
        });
    }
}
