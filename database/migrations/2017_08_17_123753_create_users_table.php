<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_name')->unique();
            $table->string('email')->unique();
            $table->string('screen_name');
            $table->string('user_colour')->default('#263238');
            $table->integer('player_class')->unsigned();
            $table->foreign('player_class')
                ->references('player_class_id')
                ->on('player_classes')
                ->onDelete('cascade');
            $table->string('password', 60);

            $table->integer('exp')->default(0);
            $table->integer('level')->default(1);
            $table->integer('vip')->default(0);
            $table->integer('access_level')->default(0);
            $table->string('profile_image')->nullable();


            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
