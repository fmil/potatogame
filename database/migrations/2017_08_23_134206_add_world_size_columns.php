<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorldSizeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('world', function (Blueprint $table) {
            $table->integer('world_vertical')->default(0);
            $table->integer('world_horizontal')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('world', function (Blueprint $table) {
            $table->dropColumn('world_vertical');
            $table->dropColumn('world_horizontal');
        });
    }
}
