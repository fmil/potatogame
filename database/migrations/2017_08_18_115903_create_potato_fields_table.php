<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePotatoFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potato_fields', function (Blueprint $table) {
            $table->increments('field_id');
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')
                ->references('user_id')
                ->on('users')
                ->ondelete('cascade');

            $table->integer('width')->default(1);
            $table->integer('height')->default(1);
            $table->integer('has_potatoes')->default(0);
            $table->enum('growth', ['seeds', 'vegetative', 'building', 'maturation', 'complete'])->default('seeds');
            $table->double('growth_ratio')->default(1);
            $table->integer("rotten_level")->default(0);
            $table->integer('field_level')->default(1);
            $table->integer('potato_id')->nullable();
            /* Mabe add some other stuff */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potato_fields');
    }
}
