<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCordFieldToRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('world_regions', function (Blueprint $table) {
            $table->integer('world_hor')->default(1);
            $table->integer('world_ver')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('world_regions', function (Blueprint $table) {
            $table->dropColumn('world_hor');
            $table->dropColumn('world_ver');
        });
    }
}
