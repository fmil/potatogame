<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePotatoStash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potato_stash', function (Blueprint $table) {
            $table->increments('potato_stash_id');
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')
                ->references('user_id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('potato_id')->unsigned();
            $table->foreign('potato_id')
                ->references('potato_id')
                ->on('potato_type')
                ->onDelete('cascade');

            $table->integer('amount')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potato_stash');
    }
}
