<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAreaIdToFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('potato_fields', function (Blueprint $table) {
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')
                ->references('area_id')
                ->on('region_areas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('potato_fields', function (Blueprint $table) {
            $table->dropForeign('area_id');
            $table->dropColumn('area_id');
        });
    }
}
