<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table("world_regions")->insert([
            "region_name" => "Brown Land", 
            "region_short_code" => "BR01LA1", 
            "world_id" => 1,
            "world_hor" => 1,
            "world_ver" => 1
        ]);
     
    }
}
