<?php

use Illuminate\Database\Seeder;

class AdminDefaultRegion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table("region_areas")->insert([
            "owner_id" => 1,
            "region_id" => 1,
            "region_short_code" => "BR01LA1",
        ]);

        DB::table("world_regions")->update([
            "unused_land" => 99,
            "used_land" => 1
        ]);
    }
}
