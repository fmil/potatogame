<?php

use Illuminate\Database\Seeder;

class WorldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table("world")->insert([
            "world_name" => "Alfa Potato",
            "description" => "Place where all alfa potatoes goes.",
            "world_vertical" => 1,
            "world_horizontal" => 1 
        ]);
        
    }
}
