<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table("users")->insert([
            "user_name" => "Admin",
            "email"  => "admin@niknais.com",
            "screen_name" => "SuperPleb",
            "player_class" => 1,
            "password" => bcrypt("iakailv123"),
            "access_level" => 100,
            "strength" => 0,
            "dexterity" => 0,
            "intelligence" => 0,
            "charisma" => 0
        ]);

    }
}
