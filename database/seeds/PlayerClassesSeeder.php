<?php

use Illuminate\Database\Seeder;

class PlayerClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $playerClasses = array(
            [
                "name" => "Average Joe",
                "short_description" => "A simple guy in simple field.",
                "long_description" => "Average Joe is simple worker who will do your deeds. He is not the smartes guy in your disposa, but he will do a job.",
                "strength" => 8,
                "dexterity" => 3,
                "intelligence" => 3,
                "charisma" =>  6 
            ],
            [
                "name" => "Utility Belt Men",
                "short_description" => "Guy has all tools he need.",
                "long_description" => "He is like Average Joe, just a bit weaker. Hovewer he will compensite his weaknes with his tools. Like batman, just for potatoes.",
                "strength" => 4,
                "dexterity" => 5,
                "intelligence" => 9,
                "charisma" =>  2 
            ],
            [
                "name" => "Millenial",
                "short_description" => "Instant gratification",
                "long_description" => "I need my potatoes now and thats all. Need everything instantly.",
                "strength" => 3,
                "dexterity" => 4,
                "intelligence" => 6,
                "charisma" =>  7 
            ],
            [
                "name" => "Bobcat",
                "short_description" => "Call me bobcat",
                "long_description" => "We are living in weird times, so why we would not have weird farmers?",
                "strength" => 5,
                "dexterity" => 10,
                "intelligence" => 2,
                "charisma" =>  3 
            ]
        );

        DB::table("player_classes")->insert(
            $playerClasses
        );

    }
}
