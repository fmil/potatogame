<?php

use Illuminate\Database\Seeder;

class PotatoTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table("potato_type")->insert([
            "potato_name" => "Basic Potato",
            "potato_description" => "Plain potato. Simple and nice",
            "selling_price" => 2,
            "buying_price" => 1,
            "growth_ratio" => 1,
            "rotting_speed" => 1 
        ]);

    }
}
