<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlayerClassesSeeder::class);
        $this->call(AdminUserSeeder::class);
        $this->call(PotatoTypeSeeder::class);

        //world and region seeds
        $this->call(WorldSeeder::class);
        $this->call(RegionSeeder::class);

        //On 5.7 $this->call([PlayerClassesSeeder::class, AdminUserSeeder::class, PotatoTypeSeeder::class]);
    }
}
