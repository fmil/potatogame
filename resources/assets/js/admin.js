/**
 * Created by kasparsvanags on 22/08/2017.
 */


$(document).ready(function () {

    $("#extend-button").on("click",function(){

        var sidebar = $("#sidebar-nav");

        if(sidebar.hasClass("extended")) {

            sidebar.animate({
                width: "50px"
            }, 500);
            sidebar.removeClass("extended");
        } else {
            sidebar.animate({
                width: "250px"
            }, 500);
            sidebar.addClass("extended");
        }
    });

    //pages manager

    $(".extends-box").on("click", function(){
        var page = $(this).data("page");

        switch (page){
            case "admin":
                window.location.replace('/potatogod');
                break;
            case "general":
                window.location.replace('/potatogod/general');
                break;
            case "map":
                window.location.replace('/potatogod/map');
                break;
            default:
                console.log(window.location.href);
                window.location.replace(window.location);
                break;
        }
    });

    //hide flash
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

});