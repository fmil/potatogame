<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 14/09/2017
 * Time: 14:59
 */

?>

@extends('auth')

@section('body')
    <div class="row" style="margin-top: 40px;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>Market place</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="well-buying">
                                @foreach($response['selling']['potatoes'] as $potato)
                                    <div class="row">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">
                                                <h4>{{ $potato['potato_name'] }} (Level {{ $potato['available_level'] }})</h4>
                                                <div>

                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-xs-9">
                                                    <p style="font-size: 1.2em; margin-bottom: 0">Price: $ <span style="color: red;">{{ $potato['buying_price'] }}</span> &nbsp;&nbsp;&nbsp; Selling price: $ <span style="color: green;">{{ $potato['selling_price'] }}</span></p>
                                                    <hr class="custom-hr">
                                                    <p style="font-size: 1.2em; margin-bottom: 0">Produced weight: {{ $potato['weight'] }}</p>
                                                </div>
                                                <div class="col-xs-3">
                                                    @if(Auth::user()->level >= $potato['available_level'])
                                                        <button class="btn btn-custom-danger buy-potato" data-price="{{$potato['buying_price']}}" data-name="{{ $potato['potato_name'] }}" data-potatoid="{{ $potato['potato_id'] }}" data-selling="{{ $potato['selling_price'] }}" data-weight="{{$potato['weight']}}">BUY</button>
                                                    @else
                                                        <button class="btn btn-custom-danger disabled">BUY</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="well-selling">
                                <div class="row">
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4>Item name ($ price)</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-xs-9">
                                                <p>Item description</p>
                                            </div>
                                            <div class="col-xs-3">
                                                <button class="btn btn-custom-secondary">SELL</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4>Item name ($ price)</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-xs-9">
                                                <p>Item description</p>
                                            </div>
                                            <div class="col-xs-3">
                                                <button class="btn btn-custom-secondary">SELL</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("auth.modals.buypotato")
@endsection

@section('scripts')

    <script type="text/javascript">


        $(".buy-potato").on("click", function () {

            var price = $(this).data("price");
            var name = $(this).data("name");
            var sellingPrice = $(this).data("selling");
            var potatoId = $(this).data("potatoid");
            var weight = $(this).data("weight");

            $("#potato-name-holder").html(name);
            $("#potato-price-holder").html("$ "+price+" (Per KG)");
            $("#potato_id").val(potatoId);

            setCalculatedValues(0,0);

            $("#selling").val(sellingPrice);
            $("#buying").val(price);
            $("#potato_weight").val(weight);

            $("#plant-potato-modal").modal("show");
        });

        $("#potato_amount").on("change", function () {
            var amount = $(this).val();
            var newSelling, newBuying, weight = $("#potato_weight").val();

            newBuying = amount * $("#buying").val();
            newSelling = (weight * amount) * $("#selling").val();

            setCalculatedValues(newBuying, newSelling);
        });


        function setCalculatedValues(buy, sell) {
            $("#potato-total-price-holder").html("$ "+buy);
            $("#potato-profit-holder").html("$ "+sell+" (Max Profit)");
        }
    </script>

@endsection
