<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 13/09/2017
 * Time: 15:24
 */

?>


<div class="modal fade" id="plant-potato-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Plant potatoes</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/plant/potato" method="post" id="create-potato-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="backpack_potato">Potato type (Amount)</label>
                                <select name="backpack_potato" id="backpack_potato" class="form-control" required>
                                    @if(array_key_exists('potatoes', $response["backpack"]))
                                        @foreach($response["backpack"]["potatoes"] as $key => $v)
                                            <option value="{{ $v['backpack_id'] }}">{{$v['item_name']}} ({{ $v['item_count'] }})</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="backpack_amount" class="control-label">Amount to use</label>
                                <input type="number" name="backpack_amount" id="backpack_amount" class="form-control" placeholder="1" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                @if(array_key_exists('potatoes', $response["backpack"]))
                                    <button type="button" class="btn btn-custom-secondary">Plant</button>
                                @else
                                    <button type="button" class="btn btn-custom-secondary disabled">Plant</button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>