<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 19/09/2017
 * Time: 15:40
 */

?>


<div class="modal fade" id="plant-potato-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Buy potatoes</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/plant/potato" method="post" id="create-potato-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="potato_id" id="potato_id">
                            <input type="hidden" name="buying" id="buying">
                            <input type="hidden" name="selling" id="selling">
                            <input type="hidden" name="weight" id="potato_weight">
                            <div class="form-group">
                                <h4 id="potato-name-holder"></h4>
                                <hr>
                                <table id="potato-info-table">
                                    <thead>
                                        <tr>
                                            <th colspan="2">INFO</th>
                                        </tr>
                                        <tr>
                                            <th>OPTION</th>
                                            <th>VALUE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>PRICE</td>
                                            <td id="potato-price-holder"></td>
                                        </tr>
                                    </tbody>
                                </table>
                       {{--         <h4 id="potato-price-holder"></h4>--}}
                            </div>
                            <div class="form-group">
                                <label for="potato_amount">Amount</label>
                                <input type="number" min="1" step="1" id="potato_amount" name="potato_amount" class="form-control" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6 danger-box">
                                        <h4 id="potato-total-price-holder">$ 0</h4>
                                    </div>
                                    <div class="col-xs-6 success-box">
                                        <h4 id="potato-profit-holder">$ 0 (Max Profit)</h4>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">BUY</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
