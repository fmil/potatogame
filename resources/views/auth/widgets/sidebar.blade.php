<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 17/08/2017
 * Time: 15:14
 */

?>

<div id="extend-button">
    <h4 class="text-center"><i class="material-icons">&#xE5D2;</i></h4>
</div>
<div id="profile-info" class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE55A;</i></h4>
</div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE84F;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE84F;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Your finances</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE8D1;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE8D1;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Your stock</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box" data-page="market">
    <h4 class="text-center"><i class="material-icons">&#xE547;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE547;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Market place</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE922;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE922;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Progress report</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE3F7;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE3F7;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Potato fields</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE80E;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE80E;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Other people fields</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE8D0;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE8D0;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Farmer top</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xEB46;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xEB46;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Chill zone</p>
        </div>
    </div>
</div>
<div class="clear"></div>
@if(Auth::user()->access_level > 99)
<div class="extends-box" data-page="admin">
    <h4 class="text-center"><i class="material-icons">&#xE869;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE869;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Admin zone</p>
        </div>
    </div>
</div>
@endif