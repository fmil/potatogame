<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 25/08/2017
 * Time: 12:22
 */

?>


<div class="modal fade" id="potato-instruction-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>What is "Potato King's"?</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                        <p>Potato King's is romantic story about world of potatoes. We give you opportunity to
                        make your dreams come true. You can become one of most influential potato field owner
                        in the world.</p>
                        <p>Is that all? You may ask. We say 'YES'. Because why you would want to do something else, if
                            potato fields are waiting you behind this beautiful register (or Login screen).
                        </p>
                        <h4>Classes</h4>
                        <p>
                            You can choose one of currently 3 available character classes. Each class has it's own default
                            power settings and in later game you can discover unique super powers for each class.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
