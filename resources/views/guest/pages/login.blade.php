<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 17/08/2017
 * Time: 10:20
 */

?>


@extends("welcome")

@section("body")

    <div class="row" id="login-container">
        <div class="col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1">
            <div class="well" id="login-form-container">
                @include('vendor.flash.message')
                <img src="/img/assets/french-fries.png" class="img-responsive center-block" width="100">
                <hr>
                <h4 class="text-center" id="login-title">Potato King's</h4>
                <div class="row">
                     <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                         <form id="login-form" action="/login" method="post">
                             {{ csrf_field() }}
                             <div class="form-group">
                                 {{-- <label for="username" class="control-label">Username</label>--}}
                                 <input type="text" id="username" name="username" class="form-control" placeholder="Username" required data-required-error="Please enter username">
                                 <div class="help-block with-errors"></div>
                             </div>
                             <div class="form-group">
                                 {{-- <label for="password" class="control-label">Password</label>--}}
                                 <input type="password" id="password" name="password" class="form-control" placeholder="Password" required data-required-error="Please enter password">
                                 <div class="help-block with-errors"></div>
                             </div>
                             <div class="form-group">
                                 <button type="submit" class="btn btn-custom-primary">Login in</button>
                                 <p class="text-center" style="margin-top: 15px; margin-bottom: 15px;">OR</p>
                                 <a href="/register" class="btn btn-custom-secondary">Sign up</a>
                                 <br>
                             </div>
                             <a href="#" style="font-size: 0.9em; margin-top: 20px;">What is "Potato King's"?</a>
                         </form>
                     </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

    <script>

        $("#login-form").validator({
            custom: {

            }
        });

    </script>
@endsection