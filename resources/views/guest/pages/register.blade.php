<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 17/08/2017
 * Time: 12:40
 */

?>

@extends("welcome")

@section("body")

    <div class="row" id="register-container">
        <div class="col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1">
            <div class="well" id="register-form-container">
                @include('vendor.flash.message')
                <img src="/img/assets/french-fries.png" class="img-responsive center-block" width="50">
                <h4 class="text-center" id="register-title">Potato King's</h4>
                <hr>
                <h4 class="text-center">Register</h4>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                        <form class="/register" id="register-form" action="/register" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Username" required data-required-error="Please enter username">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" id="screenname" name="screenname" class="form-control" placeholder="Screen name" required data-required-error="Please enter Screen name">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="email" id="email" name="email" class="form-control" placeholder="Email" required data-required-error="Please enter email address">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <select name="farmer_class" class="form-control">
                                    @foreach($response['classes'] as $class)
                                        <option value="{{ $class['player_class_id'] }}">{{ $class['name'] }}</option>
                                    @endforeach
                              {{--      <option value="1">Urban farmer</option>
                                    <option value="2">Classic farmer</option>
                                    <option value="3">Alien farmer</option>--}}
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required data-required-error="Please enter password" data-minlength="6" data-minlength-error="Password must be at least 6 characters long.">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="password" id="re-password" name="re-password" class="form-control" placeholder="Repeat Password" data-match="#password" data-match-error="Passwords doesn't match">
                                <div class="help-block with-errors"></div>
                            </div>
{{--                            <div class="form-group">
                                <label for="profile_image" class="control-label">Profile image <em>(Max 2 MB)</em></label>
                                <input type="file" name="profile_image" id="profile_image">
                            </div>--}}
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">Sign up</button>
                            </div>
                            <a href="#" data-toggle="modal" data-target="#potato-instruction-modal" style="font-size: 0.9em; margin-top: 20px;">What is "Potato King's"?</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

    <script>

    </script>
@endsection
