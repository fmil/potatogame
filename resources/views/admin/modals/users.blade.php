<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 21/08/2017
 * Time: 12:49
 */

?>

<!-- USER TYPE MODAL -->

<div class="modal fade" id="create-userclass-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Create new users class</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/potatogod/usersclass" method="post" id="create-userclass-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Class name" required data-error-required="This field is required">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="short_description" name="short_description" placeholder="Short description">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="long_description" class="control-label">long description</label>
                                <textarea class="form-control" id="long_description" name="long_description">
                                </textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="strength" class="control-label">Strength</label>
                                <input type="number" name="strength" id="strength" class="form-control" placeholder="0">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="dexterity" class="control-label">Dexterity</label>
                                <input type="number" name="dexterity" id="dexterity" class="form-control" placeholder="0">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="intelligence" class="control-label">Intelligence</label>
                                <input type="number" name="intelligence" id="intelligence" class="form-control" placeholder="0">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="charisma" class="control-label">Charisma</label>
                                <input type="number" name="charisma" id="charisma" class="form-control" placeholder="0">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-custom-secondary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="view-userclass-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>View users classes</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <table class="table table-responsive table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Str</th>
                                <th>Dex</th>
                                <th>Int</th>
                                <th>Chr</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($response['userclasses'] as $class)
                                <tr>
                                    <td>{{ $class['name'] }}</td>
                                    <td>{{ $class['strength'] }}</td>
                                    <td>{{ $class['dexterity'] }}</td>
                                    <td>{{ $class['intelligence'] }}</td>
                                    <td>{{ $class['charisma'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

