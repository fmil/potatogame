<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 21/08/2017
 * Time: 09:44
 */


?>
<!-- POTATO MODAL -->

<div class="modal fade" id="create-potato-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Create new potato type</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/potatogod/potato" method="post" id="create-potato-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control" id="potato_name" name="potato_name" placeholder="Potato name" required data-error-required="This field is required">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="potato_description" class="control-label">Description</label>
                                <textarea class="form-control" id="potato_description" name="potato_description">
                                </textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="buying_price" class="control-label">Buying price</label>
                                <input type="number" name="buying_price" id="buying_price" class="form-control" placeholder="0">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="selling_price" class="control-label">Selling price</label>
                                <input type="number" name="selling_price" id="selling_price" class="form-control" placeholder="0">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="weight" class="control-label">Weight</label>
                                <input type="number" name="weight" id="weight" class="form-control" placeholder="1">
                                <div class="help-block with-errors"></div>
                                <p><em>How much KG you can get from 1 seed</em></p>
                            </div>
                            <div class="form-group">
                                <label for="growth_ratio" class="control-label">Growth ratio</label>
                                <input type="number" name="growth_ratio" id="growth_ratio" class="form-control" placeholder="1">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="rotting_speed" class="control-label">Rotting speed</label>
                                <input type="number" name="rotting_speed" id="rotting_speed" class="form-control" placeholder="1">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="available_level" class="control-label">Available level</label>
                                <input type="number" name="available_level" id="available_level" class="form-control" placeholder="1">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="sub_products" class="control-label">Sub products</label>
                                <input type="text" name="sub_products" id="sub_products" class="form-control" placeholder="MineralX,Plutonium,Carrots,etc">
                                <div class="help-block with-errors"></div>
                                <p><em>Separate each product with , </em></p>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-custom-secondary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>