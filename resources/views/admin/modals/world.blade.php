<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 23/08/2017
 * Time: 09:55
 */

?>

{{-- Create world modal --}}

<div class="modal fade" id="create-world-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Create new world</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/potatogod/world" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="world_name" class="control-label">Name</label>
                                <input type="text" id="world_name" name="world_name" class="form-control" required data-error-required="World name is required">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="world_description" class="control-label">Description</label>
                                <textarea rows="5" cols="5" class="form-control" id="world_description" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-custom-secondary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Generate new region --}}

<div class="modal fade" id="create-region-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Generate new region</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/potatogod/region" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="region_name" class="control-label">Region name</label>
                                <input type="text" id="region_name" name="region_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="region_short_code" class="control-label">Region short code</label>
                                <p style="font-size: 0.9em">You can leave it empty it will be Auto-Generated.</p>
                                <p style="font-size: 0.9em">Pattern is [2 letters from world name, 2 digits, 2 letters from region name, 1 digit]</p>
                                <input type="text" id="region_short_code" name="region_short_code" class="form-control" placeholder="SL09RD1">
                            </div>
                            <div class="form-group">
                                <label for="world_id" class="control-label">Select World</label>
                                <select name="world_id" id="world_id" class="form-control">
                                    @foreach($response['worlds'] as $wr)
                                        <option value="{{ $wr['world_id'] }}">{{ $wr['world_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">Generate</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- Re-locate users --}}

<div class="modal fade" id="re-locate-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Create new users class</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- View area users --}}

<div class="modal fade" id="view-user-area-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Create new users class</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
