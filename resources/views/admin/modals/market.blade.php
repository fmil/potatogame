<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 22/08/2017
 * Time: 09:39
 */

?>

<div class="modal fade" id="create-market-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>Create new market type</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/potatogod/market" method="post" id="create-market-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control" id="market_name" name="market_name" placeholder="Market name" required data-error-required="This field is required">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="market_description" class="control-label">Description</label>
                                <textarea class="form-control" id="market_description" name="market_description"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="bonus_ratio" class="control-label">Bonus ratio</label>
                                <input type="number" name="bonus_ratio" id="bonus_ratio" class="form-control" value="0" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="available_level" class="control-label">Available level</label>
                                <input type="number" name="available_level" id="available_level" class="form-control" value="1">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-custom-secondary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="view-market-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="text-right close" data-dismiss="modal"><i class="material-icons">&#xE5CD;</i></p>
                <h4>View markets</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <table class="table table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Available level</th>
                                    <th>Bonus ratio</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($response['markets'] as $market)
                                <tr>
                                    <td>{{ $market['market_name'] }}</td>
                                    <td>{{ $market['available_level'] }}</td>
                                    <td>{{ $market['bonus_ratio'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>