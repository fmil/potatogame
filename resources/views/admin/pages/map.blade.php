<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 22/08/2017
 * Time: 13:36
 */

?>


@extends('admin')

@section('body')
    <div class="row" style="margin-top: 40px;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>Map settings</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <button class="btn btn-custom-secondary" data-toggle="modal" data-target="#create-region-modal">Generate new region</button>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <button class="btn btn-custom-secondary">View area</button>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <button class="btn btn-custom-secondary">Re-located users</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 40px;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>Current map</h4>
                    <p>
                        <em><i class="material-icons" style="color: #CFD8DC;">&#xE047;</i> Area is unused
                            <i class="material-icons" style="color: #43A047;">&#xE047;</i> Area is user
                            <i class="material-icons" style="color: #FF5722;">&#xE047;</i> Area is fake
                        </em>
                    </p>
                </div>
                <div class="panel-body">
                    @if(!$response['has_world'])
                    <button class="btn btn-custom-secondary" data-toggle="modal" data-target="#create-world-modal">Create new world</button>
                    @else
                        @foreach($response['worlds'] as $wr)
                            <h4>{{ $wr['world_name'] }}</h4>
                            <div id="world-map">
                                <div class="world-zones">
                                    <?php $i=0; $total=0; ?>

                                @foreach($response['regions'][$wr['world_id']] as $reg)
                                    <?php $i++; $total++; ?>
                                    <div class="world-region-block">
                                        <div class="well">
                                            @for($i=1; $i<101; $i++)
                                                <?php
                                                    $cur_ver =  ceil($i/10);
                                                    $cur_hor = $i-(10*($cur_ver-1));
                                                ?>
                                                @if(array_key_exists($reg['region_id'].'/'.$cur_hor.'/'.$cur_ver, $response['region_areas']))
                                                    <div class="small-region-box" style="background-color: #43A047;">
                                                    </div>
                                                @else
                                                    <div class="small-region-box" style="background-color: #CFD8DC;">
                                                    </div>
                                                @endif
                                            @endfor
                                        </div>
                                    </div>
                                    <?php
                                        if($i == 10) {
                                            echo '</div>';
                                            echo '<div class="clear">';
                                            echo '<div class="world-zones">';
                                        }

                                        if($total == count($response['regions'][$wr['world_id']])) {
                                            echo '</div>';
                                        }
                                    ?>
                                @endforeach
                          {{--          <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                    <div class="world-region-block">
                                        <div class="well"></div>
                                    </div>
                                </div>
                                <div class="clear">
                                    <div class="world-zones">
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                        <div class="world-region-block">
                                            <div class="well"></div>
                                        </div>
                                    </div>
                                </div>--}}
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('admin.modals.world')

@endsection

@section('scripts')

@endsection
