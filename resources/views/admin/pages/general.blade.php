<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 22/08/2017
 * Time: 13:15
 */

?>

@extends('admin')

@section('body')
    <div class="row" style="margin-top: 40px;">
        <div class="col-xs-12 col-sm-3 col-sm-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h4>Potato manager</h4>
                </div>
                <div class="panel-body">
                    <button type="button" class="btn btn-custom-secondary" data-toggle="modal" data-target="#create-potato-modal">Create potato</button>
                    <hr>
                    {{--   <p>Potato list</p>--}}
                    <table class="table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Available level</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($response['potato'] as $potato)
                            <tr>
                                <td>{{ $potato['potato_name'] }}</td>
                                <td>{{ $potato['available_level'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-7">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h4>General stuff</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <button type="button" class="btn btn-custom-primary" data-toggle="modal" data-target="#create-userclass-modal"> Create user class</button>
                            <button type="button" class="btn btn-blank btn-custom-primary" data-toggle="modal" data-target="#view-userclass-modal" style="margin-top: 10px;"> View user classes</button>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <button type="button" class="btn btn-custom-primary" data-toggle="modal" data-target="#create-market-modal">Create market type</button>
                            <button type="button" class="btn btn-blank btn-custom-primary" data-toggle="modal" data-target="#view-market-modal" style="margin-top: 10px;"> View markets</button>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <button type="button" class="btn btn-custom-primary disabled">Create market product</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--All modals for admin panel --}}
    @include("admin.modals.potato")
    @include("admin.modals.users")
    @include("admin.modals.market")

@endsection

@section('scripts')

@endsection

{{--

"Road side", "Car booth",
 "Door to Door", "Restaurants", "Farmer market",
"Local convenience store", "One supermarket", "Supermarket chain", "Expensive supermarkets",
"VIP (potatoes goes as bonuses for cars, cloths, etc)"

--}}

