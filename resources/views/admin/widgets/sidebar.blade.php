<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 18/08/2017
 * Time: 15:39
 */

?>

<div id="extend-button">
    <h4 class="text-center"><i class="material-icons">&#xE5D2;</i></h4>
</div>
<div id="profile-info" class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE55A;</i></h4>
</div>
<div class="clear"></div>
<div class="extends-box" data-page="admin">
    <h4 class="text-center"><i class="material-icons">&#xE01D;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE01D;</i></p>
        </div>
        <div class="menu-box-text">
            <p>General info</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box" data-page="general">
    <h4 class="text-center"><i class="material-icons">&#xE8B9;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE8B9;</i></p>
        </div>
        <div class="menu-box-text">
            <p>General settings</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box" data-page="map">
    <h4 class="text-center"><i class="material-icons">&#xE80B;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE80B;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Map settings</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE02F;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE02F;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Manage news</p>
        </div>
    </div>
</div>
{{--<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE3F1;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE3F1;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Manage potato types</p>
        </div>
    </div>
</div>--}}
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE547;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE547;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Manage stores</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE616;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE616;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Manage events</p>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="extends-box">
    <h4 class="text-center"><i class="material-icons">&#xE53E;</i></h4>
    <div class="menu-box">
        <div class="menu-box-icon">
            <p class="text-center"><i class="material-icons">&#xE53E;</i></p>
        </div>
        <div class="menu-box-text">
            <p>Manage finances</p>
        </div>
    </div>
</div>



