<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 17/08/2017
 * Time: 14:36
 */

?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>POTATO KING'S</title>

    <!-- Fonts -->
    <link href="{{ URL::asset("css/app.css") }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,,500,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ URL::asset("js/jquery.min.js") }}" type="text/javascript"></script>
    <script src="{{ URL::asset("js/bootstrap.js") }}" type="text/javascript"></script>
    <script src="{{ URL::asset("js/main.js") }}" type="text/javascript"></script>

    <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div id="sidebar-nav">
            @include("auth.widgets.sidebar")
        </div>
        <div class="col-xs-12" id="main-content">
            @include('vendor.flash.message')
            @yield("body")
        </div>
    </div>
    <div class="footer-container">
        <p class="text-center" style="margin-bottom: 0px;">
            2017 © POTATO KING'S
        </p>
    </div>
</div>
@yield("scripts")
</body>
</html>

