
## About Potato Game

You can't break love of easter europien and his potatoes. This game is about struggle to be the best potato maker in neighborhood.
It is not a choice, it is a life style.

## Game Provides

- Making your own potato empire
- Compite with other potato makers
- Create aliances with other farmers
- Play mini games 


## Current game status

At the moment game is in good place. Place of abyse. We know how worlds will work and how potatoes looks.

### Things we need for first alfa test

There is few things, after which will be done, we can say that game is **v0.0.1**

- Finish region generation
- Create multiple players in same region
- Make these player visible for others

For **v0.0.2** we need to add: 

- Individual field system
- Working potato market (at least buying potatoes)

For **v0.0.3** we need to add:

- Option to communicate with other players
- Can sell potatoes (if not done on **v0.0.2**)
- Create global rankings based on field sizes


### Future

At this point we could go **v0.1.0** and then see where wind blows
**Come on... we all know this wont see daylight...**
